mock_utils
==========

Python package providing utilities for mocking stuff on Python.

Installing
----------

```bash
$ pip install mock_utils
```

Building documentation
----------------------

Provided you have everything listed in `dev_requirements.txt`,
run:

```bash
$ python build_docs.py
```

Running tests
-------------

Use `nose`:

```bash
$ nosetests
```

For coverage reports:

```bash
$ nosetests --with-coverage --cover-erase --cover-package src
```
