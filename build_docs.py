"""Script for building documentation for the project.

Usage:
    python build_docs.py

Notes:
    If the "doc" folder doesn't exist, the script will generate it, but
    the conf.py file will need to be edited, and the script rerun. Remember
    to:

    * Add the napoleon extension.
    * Add src/ to sys.path.
    * Set autoclass_content = 'both' to generate constructor documentation.
"""

import sys
import fnmatch

from os import path, makedirs, walk

from sphinx.apidoc import main as apidoc
from sphinx import main as build

# Configs:

exclusion_filters = ['*test.py']

# Code:

exclusions = []
for root, dirnames, filenames in walk('src'):
    for filter in exclusion_filters:
        for filename in fnmatch.filter(filenames, filter):
              exclusions.append(path.join(root, filename))

apidoc_args = ['', '-e', '-F', '-o', 'doc', 'src', '-H', 'mock_utils'] + exclusions
build_args = ['', '-b', 'html', 'doc', path.join('doc', '_build')]

if not path.exists('doc'):
    print("Warning: no doc folder found. Generating a doc folder...")
    apidoc(apidoc_args)
    print("Edit doc/conf.py as needed and rerun this script. Refer to "
          "this script's docstring for the changes needed.")
    exit(-1)

apidoc(apidoc_args)
build(build_args)
